var widgets = require("widget");
var tabs = require("tabs");
 
var widget = widgets.Widget({
  id: "smmry-this-link",
  label: "Smmry This Page",
  contentURL: "http://smmry.com/favicon.ico",
  onClick: function() {
    tabs.open("http://smmry.com/" + tabs.activeTab.url);
  }
});