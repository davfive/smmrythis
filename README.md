Smmry This
===========

Overview
---------
The Smmry This plugin takes the current page and opens it using Smmry.com. The initial reason for this add-on is because I was looking for a way to easily forward links to friends for webpages that I am on that are nicely laid out summaries that they could then read and click on the link for the full story.

How To Use
-----------
The Smmry.com arrow will appear in the lower right status area of firefox. When you get to a page that you want to summarize, simply click the arrow.